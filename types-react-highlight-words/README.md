# Installation
> `npm install --save @types/react-highlight-words`

# Summary
This package contains type definitions for react-highlight-words (https://github.com/bvaughn/react-highlight-words#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-highlight-words.

### Additional Details
 * Last updated: Thu, 23 Dec 2021 23:35:37 GMT
 * Dependencies: [@types/react](https://npmjs.com/package/@types/react)
 * Global values: none

# Credits
These definitions were written by [Mohamed Hegazy](https://github.com/mhegazy), and [Kelly Milligan](https://github.com/kellyrmilligan).
